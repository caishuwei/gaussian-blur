//
// Created by Dell on 2021/5/27.
//
#include "ggl.h"

#ifndef FFMPEGTEST_UTILS_H
#define FFMPEGTEST_UTILS_H
unsigned char * LoadFileContent(AAssetManager *aAssetManager ,const char *path , int &fileSize);

GLuint CompileShader(GLenum shaderType , const char * shaderCode);

GLuint CreateProgram(GLuint vsShader , GLuint fsShader);

GLuint CreateTextureFromBMP(AAssetManager *aAssetManager ,const char * bmpPath);

GLuint CreateTexture2D(unsigned char *pixelData, int Width, int height ,GLenum type);
#endif //FFMPEGTEST_UTILS_H
