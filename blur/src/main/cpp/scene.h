//
// Created by Dell on 2021/5/27.
//
#include "utils.h"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/ext.hpp"
#include "glm/detail/_noise.hpp"

#ifndef FFMPEGTEST_SCENE_H
#define FFMPEGTEST_SCENE_H

void Init();

void SetViewPortSize(float width, float height);

void Draw();

class Scene {

public:

    //构造函数
    Scene(AAssetManager *aAssetManager) {
        this->aAssetManager = aAssetManager;
    }

    //析构函数
    ~Scene() {
        this->aAssetManager = nullptr;
    }

    //初始化
    virtual void init() = 0;

    //设置视图窗口大小
    virtual void setViewPortSize(float width, float height) {
        glViewport(0, 0, width, height);
    }

    //刷新一帧
    virtual void draw() = 0;

    /**
     * 设置旋转角度
     */
    virtual void setRotate(float angle) {

    }

protected:
    AAssetManager *aAssetManager;      // android AssetManager
};

#endif //FFMPEGTEST_SCENE_H
