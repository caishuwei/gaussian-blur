//
// Created by Dell on 2021/6/1.
//
#include <jni.h>
#include "../utils.h"

#ifndef FFMPEGTEST_FORMATUTILS_H
#define FFMPEGTEST_FORMATUTILS_H

unsigned char *ConvertJByteArrayToChars(JNIEnv *env, jbyteArray byteArray);


#endif //FFMPEGTEST_FORMATUTILS_H
