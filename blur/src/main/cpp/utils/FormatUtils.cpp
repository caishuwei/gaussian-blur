//
// Created by Dell on 2021/6/1.
//

#include "FormatUtils.h"
#include <cstring>

unsigned char *ConvertJByteArrayToChars(JNIEnv *env, jbyteArray byteArray) {
    unsigned char *chars;
    jbyte *bytes = env->GetByteArrayElements(byteArray, 0);
    int chars_len = env->GetArrayLength(byteArray);
//    LOGI("获取jbyteArray长度 %d",chars_len);
    chars = new unsigned char[chars_len + 1];
    memset(chars, 0, chars_len + 1);
    memcpy(chars, bytes, chars_len);
    chars[chars_len] = 0;
    env->ReleaseByteArrayElements(byteArray, bytes, 0);
    return chars;
}