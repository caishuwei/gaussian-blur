#include <unistd.h>
#include "ggl.h"
#include "utils.h"
#include "scene.h"
#include "utils/FormatUtils.h"
#include "BlurPicture.cpp"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
} // endof extern "C"
#endif

/**
 * 加载默认回调
 * @param vm
 * @param reserved
 * @return
 */
jint JNI_OnLoad(JavaVM *vm, void *reserved) {
    JNIEnv *env = NULL;
    if (vm->GetEnv((void **) &env, JNI_VERSION_1_6) != JNI_OK) {
        return -1;
    }
    return JNI_VERSION_1_6;
}

BlurPicture *scene = nullptr;


extern "C"
JNIEXPORT void JNICALL
Java_com_csw_android_blur_BlurPictureRenderer_initScene(JNIEnv *env, jobject thiz,
                                                        jobject asset_manager) {
    AAssetManager *aAssetManager = AAssetManager_fromJava(env, asset_manager);
    scene = new BlurPicture(aAssetManager);
}

extern "C"
JNIEXPORT void JNICALL
Java_com_csw_android_blur_BlurPictureRenderer_nativeOnSurfaceCreated(JNIEnv *env, jobject thiz) {
    scene->init();
}

extern "C"
JNIEXPORT void JNICALL
Java_com_csw_android_blur_BlurPictureRenderer_nativeOnSurfaceChanged(JNIEnv *env, jobject thiz,
                                                                     jint width, jint height) {
    scene->setViewPortSize(width, height);
}

extern "C"
JNIEXPORT void JNICALL
Java_com_csw_android_blur_BlurPictureRenderer_nativeOnDrawFrame(JNIEnv *env, jobject thiz) {
    scene->draw();
}

extern "C"
JNIEXPORT void JNICALL
Java_com_csw_android_blur_BlurPictureRenderer_setRGBPicture(JNIEnv *env, jobject thiz, jint width,
                                                            jint height, jbyteArray rgb_bytes) {
    unsigned char *byteData = ConvertJByteArrayToChars(env, rgb_bytes);
    scene->setPictureData(width, height, byteData);
}