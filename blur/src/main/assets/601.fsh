#version 300 es

precision mediump float;
//2d纹理id
layout (location = 5) uniform sampler2D U_texture;
//当前片段对应的纹理坐标
in vec2 V_Texcoord;
//输出片段颜色
out vec4 fragColor;

vec4 gaussianBlur(vec2 position);
vec4 gaussianBlur2(vec2 position);
vec4 getRGB(vec2 position);

void main() {
    //根据当前坐标从纹理读取对应的颜色
    fragColor = gaussianBlur2(V_Texcoord);
}

//高斯模糊，使用高斯分布给周围点颜色加权重，根据权重来计算颜色值
vec4 gaussianBlur(vec2 position){
    //对x,y方向的点进行加权计算像素值
    float radius = 3.0/360.0;
    int row = 9;
    int center = row/2;
    float step = radius/float(center);
    vec2 uv0=position;
    vec4 sum = vec4(0.0, 0.0, 0.0, 1.0);
    float weightSum = 4.0+4.0*4.0+4.0*4.0+2.0*4.0+1.0*4.0;
    float weight = 4.0;
    sum += getRGB(uv0)*(weight/weightSum)*(weight/weightSum);
    for (int x = 1;x<=center;x++){
        if (x == 1){
            weight = 4.0;
        } else if (x == 2){
            weight = 4.0;
        } else if (x == 3){
            weight = 2.0;
        } else {
            weight = 1.0;
        }
        sum += getRGB(uv0 + vec2(step*float(x), 0.0))*(weight/weightSum);
        sum += getRGB(uv0 + vec2(step*float(-x), 0.0))*(weight/weightSum);
    }
    for (int y = 1;y<=center;y++){
        if (y == 1){
            weight = 4.0;
        } else if (y == 2){
            weight = 4.0;
        } else if (y == 3){
            weight = 2.0;
        } else {
            weight = 1.0;
        }
        sum += getRGB(uv0 + vec2(0.0, step*float(y)))*(weight/weightSum);
        sum += getRGB(uv0 + vec2(0.0, step*float(-y)))*(weight/weightSum);
    }
    return vec4(sum.rgb, 1.0);
}
vec4 gaussianBlur2(vec2 position){
    //对5x5的像素点进行加权计算颜色值
    float radius = 3.0/360.0;
    int row = 5;
    int center = row/2;
    float step = radius/float(center);
    vec2 uv0=position;
    vec4 sum = vec4(0.0, 0.0, 0.0, 1.0);
    //    float weightSum = 2.0+2.0*8.0+1.0*16.0;
    //    float weight = 0.0;
    //    for (int x = 0;x<row;x++){
    //        for (int y = 0;y<row;y++){
    //            if (x-center == 0 && y - center == 0){
    //                weight = 2.0;
    //            } else if ((x-center == 1 || x-center == -1)&& (y - center == 1||y-center==-1)){
    //                weight = 2.0;
    //            } else {
    //                weight = 1.0;
    //            }
    //            sum += getRGB(uv0 + vec2(step*float(x-center), step*float(y-center)))*(weight/weightSum);
    //        }
    //    }

    //将上面的计算过程优化，去除一些不必要的计算
    float weight1 = 0.02941;
    float weight2 = 0.05882;
    float offsets[5];
    offsets[0] = step*-2.0;
    offsets[1] = step*-1.0;
    offsets[2] = 0.0;
    offsets[3] = step;
    offsets[4] = step*2.0;
    for (int x = 0;x<row;x++){
        for (int y = 0;y<row;y++){
            if (x == 4 || x == 0 || y == 4 || y == 0){
                sum += getRGB(vec2(uv0.x+offsets[x], uv0.y+offsets[y])) * weight1;
            } else {
                sum += getRGB(vec2(uv0.x+offsets[x], uv0.y+offsets[y])) * weight2;
            }
        }
    }
    return vec4(sum.rgb, 1.0);
}
vec4 getRGB(vec2 position){
    return texture(U_texture, position);
}

