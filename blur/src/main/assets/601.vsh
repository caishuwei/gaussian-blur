#version 300 es

//定义变幻矩阵，最常用的就是这三种变换矩阵，根据需要也可以定义更多类型的
layout (location = 0) uniform mat4 ProjectionMatrix;
layout (location = 1) uniform mat4 ViewMatrix;
layout (location = 2) uniform mat4 ModelMatrix;

//顶点坐标及纹理坐标
layout (location = 3) in vec4 position;
layout (location = 4) in vec2 texcoord;
//输出纹理坐标到片段着色器
out vec2 V_Texcoord;

void main() {
    //对顶点坐标依次应用场景变换，视窗变换，模型变换
    gl_Position = ProjectionMatrix*ViewMatrix*ModelMatrix*position;
    V_Texcoord = texcoord;
}
