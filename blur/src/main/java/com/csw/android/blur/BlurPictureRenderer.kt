package com.csw.android.blur

import android.content.Context
import android.content.res.AssetManager
import android.opengl.GLSurfaceView
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

open class BlurPictureRenderer(context: Context) : GLSurfaceView.Renderer {

    companion object {
        init {
            System.loadLibrary("GLV3Renderer-lib")
        }
    }

    private external fun initScene(assetManager: AssetManager)
    private external fun nativeOnSurfaceCreated()
    private external fun nativeOnSurfaceChanged(width: Int, height: Int)
    private external fun nativeOnDrawFrame()
    private external fun setRGBPicture(width: Int, height: Int, rgbBytes: ByteArray)
    init {
        initScene(context.assets)
    }

    override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {
        nativeOnSurfaceCreated()
    }

    override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
        nativeOnSurfaceChanged(width, height)
    }

    override fun onDrawFrame(gl: GL10?) {
        nativeOnDrawFrame()
    }

    fun updatePicture(width: Int, height: Int, rgbBytes: ByteArray) {
        setRGBPicture(width, height, rgbBytes)
    }

}