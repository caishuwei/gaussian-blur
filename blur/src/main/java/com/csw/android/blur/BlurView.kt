package com.csw.android.blur

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.opengl.GLSurfaceView
import android.util.AttributeSet
import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import javax.microedition.khronos.opengles.GL10

/**
 * 高斯模糊实现
 */
class BlurView : GLSurfaceView, LifecycleObserver {
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    var inDisplay: Boolean = false
        set(value) {
            if (field == value) {
                return
            }
            field = value
            checkStateChange()
        }
    var inResume: Boolean = false
        set(value) {
            if (field == value) {
                return
            }
            field = value
            checkStateChange()
        }
    private val blurPictureRenderer = object : BlurPictureRenderer(context) {
        override fun onDrawFrame(gl: GL10?) {
            //开始绘制一帧前，先采集图像
            targetView?.run {
                var w = width
                var h = height
                if (w > 0 && h > 0) {
//                    w /= 4
//                    h /= 4
                    var bitmap: Bitmap? = null
                    try {
                        //绘制
                        bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
                        val canvas = Canvas(bitmap)
//                        canvas.scale(1/4f, 1/4f)
                        draw(canvas)
                        //数据提取
                        val pixels = IntArray(w * h)
                        bitmap.getPixels(pixels, 0, w, 0, 0, w, h)
                        val rgbByteArray = ByteArray(pixels.size * 3)
                        var p: Int
                        for (i in pixels.indices) {
                            p = pixels[i]
                            rgbByteArray[i * 3 + 0] = p.shr(16).and(0xff).toByte()
                            rgbByteArray[i * 3 + 1] = p.shr(8).and(0xff).toByte()
                            rgbByteArray[i * 3 + 2] = p.and(0xff).toByte()
                        }
                        updatePicture(w, h, rgbByteArray)
                    } catch (e: Exception) {
                        throw e
                    } finally {
                        bitmap?.recycle()
                    }
                }
            }
            super.onDrawFrame(gl)
        }
    }
    var targetView: View? = null
        set(value) {
            field = value
        }

    init {
        setEGLContextClientVersion(3)
        setRenderer(blurPictureRenderer)
//        renderMode = RENDERMODE_WHEN_DIRTY
    }

    private fun checkStateChange() {
        if (inDisplay && inResume) {
            onResume()
        } else {
            onPause()
        }
    }
    //----------------------------------------------------------------------------------------------

    fun setLifecycle(lifecycle: Lifecycle) {
        if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) {
            inResume = true
        }
        lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    private fun onUIResume() {
        inResume = true
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    private fun onUIPause() {
        inResume = false
    }
}