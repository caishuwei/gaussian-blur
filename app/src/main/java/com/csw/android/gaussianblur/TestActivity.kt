package com.csw.android.gaussianblur

import android.graphics.Color
import android.os.Bundle
import android.view.Gravity
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import com.csw.android.blur.BlurTargetLayout
import com.csw.android.blur.BlurView

class TestActivity : AppCompatActivity() {

    private lateinit var blurView: BlurView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ui_main)
        val drawerLayout = findViewById<DrawerLayout>(R.id.drawerlayout)
        val image = findViewById<View>(R.id.image)
        val image2 = findViewById<View>(R.id.image2)
        val blurTargetLayout = findViewById<View>(R.id.blurTargetLayout)

        image2.setOnClickListener {
            drawerLayout.openDrawer(Gravity.LEFT)
        }

        blurView = findViewById(R.id.blurView)
        blurView.setLifecycle(lifecycle)
        blurView.targetView = blurTargetLayout

        val animTask = object : Runnable {
            override fun run() {
//                image.translationY = image.translationY + 1
//                if (image.translationY >= 300) {
//                    image.translationY = 0f
//                }
                image2.rotation += 5
                image2.rotation %= 360

                image.postDelayed(this, 16)
            }
        }
        image.post(animTask)

        drawerLayout.addDrawerListener(object : DrawerLayout.DrawerListener {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                //drawerLayout拖拽过程中View逐渐显示，这里将View偏移，
                blurView.translationX = blurView.width * (1 - slideOffset)
                blurView.inDisplay = slideOffset != 0.0f
            }

            override fun onDrawerOpened(drawerView: View) {

            }

            override fun onDrawerClosed(drawerView: View) {
            }

            override fun onDrawerStateChanged(newState: Int) {
            }
        })
    }

}